import Link from "next/link";
import styles from "../styles/Home.module.scss";

export default function NotPage() {
  return (
    <div className={styles.heading}>
      <h1>404 Error Page</h1>
      <div>
        <h3 className={styles.please}>
          Please, go to{" "}
          <Link href={"/"}>
            <div className={styles.here}>Here</div>
          </Link>
        </h3>
      </div>
    </div>
  );
}
