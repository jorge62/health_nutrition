import { Fragment, useEffect, useState } from "react";
import { Header } from "../../src/components/layout/header";
import styles from "./styles.module.scss";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import Meta from "../../src/components/elements/Meta/meta";
import { useRouter } from "next/router";
import Router from "next/router";
import Link from "next/link";

export default function Articles(data) {
  const [validData, setValidData] = useState(true);
  const [date, setDate] = useState({ anio: "", mes: "", dia: "" });
  const router = useRouter();

  const mes = [
    "enero",
    "febrero",
    "mazro",
    "abril",
    "mayo",
    "junio",
    "julio",
    "agosto",
    "septiembre",
    "octubre",
    "noviembre",
    "diciembre",
  ];

  useEffect(() => {
    if (JSON.stringify(data) !== "{}") {
      setValidData(false);
    }
    if (typeof data.id !== "undefined") {
      let yymmdd = data.published.split("T")[0];
      let yy = yymmdd.split("-")[0];
      let mm = yymmdd.split("-")[1];
      let dd = yymmdd.split("-")[2];

      mes.map((e, index) => {
        if (index === parseInt(mm)) mm = e;
      });
      setDate({ anio: yy, mes: mm, dia: dd });
    }
  }, []);

  return (
    <Fragment>
      <Meta
        // ogImage={data.featured_media !== null ? data.featured_media.medium : ""}
        ogType={"articulo"}
        ogUrl={"localhost:3000" + router.basePath + router.asPath}
      />
      {typeof data.data !== "undefined" && data.data.status === 404 ? (
        <div>
          <Link href={"/"}>
            <h1>
              <a className={styles.notProduct}>
                No existe el articulo. Click aqui
              </a>
            </h1>
          </Link>
        </div>
      ) : (
        <div>
          <Header />
          <div className={styles.container}>
            <h2 className={styles.title}>{data.title}</h2>
            {validData
              ? ""
              : data.categories.map((c, index) => {
                  return (
                    <div className={styles.category} key={index}>
                      <span className={styles.categoryTitle}>Categoria: </span>
                      <span className={styles.categoryTitle}> {c.name}</span>
                      <p>{c.description}</p>
                      <div className={styles.clickAca}>
                        <a
                          className={styles.categoryLink}
                          href={c.link}
                          target="_blank"
                          rel="noreferrer"
                        >
                          Click aca para mas informacion{" "}
                        </a>
                      </div>
                    </div>
                  );
                })}

            <div className={styles.fecha}>
              <span>Fecha de publicacion: </span>
              <span> {date.dia} de </span>
              <span> {date.mes}, </span>
              <span> {date.anio}</span>
            </div>

            <div className={styles.author}>
              <div className={styles.authorNameBox}>
                <span>Autor/a: </span>
                <a href={data.author.link} target="_blank" rel="noreferrer">
                  <span className={styles.authorName}>{data.author.name}</span>
                </a>
              </div>
              <img
                src={data.author.picture}
                className={styles.imageAuthor}
              ></img>
              <p className={styles.authorResume}>
                {data.author.summary !== null
                  ? data.author.summary.replace(/<\/?[^>]+(>|$)/g, "")
                  : ""}
              </p>
            </div>

            <div className={styles.img}>
              <img
                src={
                  data.featured_media !== null ? data.featured_media.medium : ""
                }
              ></img>
            </div>
            <div className={styles.content}>
              {data.content.replace(/<\/?[^>]+(>|$)/g, "")}
            </div>

            <p className={styles.nextPostTitle}>Pr&oacute;ximos posts</p>
            <div className={styles.nextPost}>
              {data.next_posts !== null
                ? data.next_posts.map((p, index) => {
                    return (
                      <div className={styles.cardNexPost} key={index}>
                        <CardActionArea>
                          <Card sx={{ width: 330, height: 300 }}>
                            {p.featured_media === null ? (
                              ""
                            ) : (
                              <CardMedia
                                component="img"
                                height="120"
                                image={p.featured_media.medium}
                                alt="green iguana"
                              />
                            )}
                            <CardContent>
                              <Typography
                                gutterBottom
                                variant="h7"
                                component="div"
                              >
                                {p.title}
                              </Typography>
                              <Typography
                                variant="body2"
                                color="text.secondary"
                              >
                                {p.headline}
                              </Typography>
                            </CardContent>
                          </Card>
                        </CardActionArea>
                      </div>
                    );
                  })
                : ""}
            </div>
          </div>
        </div>
      )}
    </Fragment>
  );
}

export async function getStaticPaths() {
  try {
    const res = await fetch(
      "https://beta.mejorconsalud.com/wp-json/mc/v3/posts"
    );
    const data = await res.json();
    const paths = data.data.map(({ id }) => ({ params: { id: `${id}` } }));
    return {
      paths,
      fallback: "blocking",
    };
  } catch (error) {
    console.log(error);
  }
}

export async function getStaticProps(paths) {
  try {
    const res = await fetch(
      "https://beta.mejorconsalud.com/wp-json/mc/v3/posts/" + paths.params.id
    );
    const data = await res.json();
    return {
      props: data,
    };
  } catch (error) {}
}
