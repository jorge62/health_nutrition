import Head from "next/head";
import { Header } from "../src/components/layout/header";
import styles from "../styles/Home.module.scss";
import { Provider } from "react-redux";
import store from "../src/store";
import dynamic from "next/dynamic";

const Search = dynamic(() => import("../src/components/elements/Search"), {
  loading: () => <h1>Loading...</h1>,
  ssr: false,
});

export default function Home() {
  return (
    <Provider store={store}>
      <div className={styles.container}>
        <Head>
          <title>Salud y nutricion</title>
          <meta
            name="description"
            content="Sitio salud y nutricion saludable"
          />
          <meta property="og:title" content="Vida saludable" />
        </Head>
        <Header />
        <Search />
      </div>
    </Provider>
  );
}
