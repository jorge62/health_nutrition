import Head from "next/head";

const Meta = ({
  title,
  keywords,
  description,
  ogTitle,
  ogType,
  ogUrl,
  ogImage,
}) => {
  return (
    <Head>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      ></meta>
      <meta name="keywords" content={keywords}></meta>
      <meta property="og:title" content={ogTitle} />
      <meta property="og:type" content={ogType} />
      <meta property="og:image" content={ogImage} />
      <meta property="og:description" content={description} />
      <meta property="og:url" content={ogUrl} />
      <meta charSet="utf-8"></meta>
      <title>{title}</title>
    </Head>
  );
};
Meta.defaultProps = {
  title: "Vida saludable",
  keywords: "salud bienestar vegetales vida saludable",
  description:
    "En este sitio podras mejorar e incluso encontrar todo sobre una vida saludable",
  ogTitle: "contenido personalizado de salud y nutricion",
};
export default Meta;
