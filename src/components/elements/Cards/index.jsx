import { Fragment } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea, duration } from "@mui/material";
import Link from "next/link";
import styles from "./styles.module.scss";
import Meta from "../Meta/meta";

export default function Cards({
  list,
  loading,
  page,
  rowsPerPage,
  setLoading,
}) {
  return (
    <Fragment>
      {list.length !== 0 && loading === false
        ? list
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((p, index) => {
              return (
                <div className={styles.cardProducts} key={index}>
                  <Meta
                    ogImage={
                      p.featured_media === null ? "" : p.featured_media.medium
                    }
                  />
                  <CardActionArea>
                    <Link href={`/Article/${p.id}`}>
                      <Card
                        style={{
                          padding: "8px",
                        }}
                        sx={{ width: 330, height: 300 }}
                        onClick={() => {
                          setLoading(true);
                        }}
                      >
                        {p.featured_media === null ? (
                          ""
                        ) : (
                          <CardMedia
                            component="img"
                            height="120"
                            image={p.featured_media.medium}
                            alt="green iguana"
                          />
                        )}
                        <CardContent>
                          <Typography gutterBottom variant="h7" component="div">
                            {p.title}
                          </Typography>
                          <Typography variant="body2" color="text.secondary">
                            {p.headline}
                          </Typography>
                        </CardContent>
                      </Card>
                    </Link>
                  </CardActionArea>
                </div>
              );
            })
        : ""}
    </Fragment>
  );
}
