import React, { Fragment, useEffect, useState, useRef } from "react";
import styles from "./styles.module.scss";
import TextField from "@mui/material/TextField";
import { MdSearch } from "react-icons/md";
import clienteAxios from "../../../api/axios";
import Swal from "sweetalert2";
import { FaFilter } from "react-icons/fa";
import { DotLoader } from "react-spinners";
import Pagination from "@mui/material/Pagination";
import { useDispatch, useSelector } from "react-redux";
import {
  setLastArt,
  setListProducts,
  setSearchProd,
} from "../../../store/slices/products";
import dynamic from "next/dynamic";
import Meta from "../Meta/meta";
import { useRouter } from "next/router";

const Cards = dynamic(() => import("../Cards"), {
  ssr: true,
});

const override = {
  display: "block",
  borderColor: "red",
};

export default function Search({ data }) {
  const [busqueda, guardarBusqueda] = useState("");
  const [loading, setLoading] = useState(false);
  const [dataPage, setDataPage] = useState(0);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const cardRef = useRef();
  const router = useRouter();

  const dispatch = useDispatch();

  const { list } = useSelector((state) => state.products);
  const { searchProd } = useSelector((state) => state.products);
  const { lastArt } = useSelector((state) => state.products);

  useEffect(() => {
    guardarBusqueda(searchProd);
    setDataPage(0);
    setPage(0);
  }, [loading, searchProd, cardRef]);

  const buscarProducto = async (e) => {
    dispatch(setSearchProd(busqueda));
    if (e.key === "Enter" || e.keyCode === 13 || e.type === "click") {
      e.preventDefault();
      dispatch(setLastArt(false));
      setLoading(true);
      await clienteAxios
        .get(
          `/posts?search=${busqueda.replace(/\s/g, "").trim().toLowerCase()}`
        )
        .then(async (res) => {
          setLoading(false);
          if (res.data.data.length === 0) {
            dispatch(setListProducts([]));
            Swal.fire({
              type: "error",
              title: "Error",
              text: "¡No hay artículos relacionados con el término de búsqueda!",
              width: "600px",
              confirmButtonText: "Aceptar!",
            }).then((result) => {
              if (result.isConfirmed) {
                buscarOtros();
              }
            });
          } else if (res.data.data.status === 404) {
            dispatch(setListProducts([]));
            Swal.fire({
              type: "error",
              title: "Error",
              text: res.data.message,
              width: "600px",
            });
          } else {
            dispatch(setListProducts(res.data.data));
          }
        })
        .catch((e) => {
          setLoading(false);
          Swal.fire({
            type: "error",
            title: "Error",
            text: e,
            width: "600px",
          });
        });
    }
  };

  const buscarOtros = async () => {
    dispatch(setLastArt(true));
    const res = await fetch(
      "https://beta.mejorconsalud.com/wp-json/mc/v3/posts?orderby=date&order=desc"
    );
    const data = await res.json();
    dispatch(setListProducts(data.data));
  };

  const filterRelevant = async (e) => {
    dispatch(setSearchProd(busqueda));
    if (e.type === "click") {
      e.preventDefault();
      dispatch(setLastArt(false));
      setLoading(true);
      await clienteAxios
        .get(
          `/posts?search=${busqueda
            .replace(/\s/g, "")
            .trim()
            .toLowerCase()}&page=1&orderby=relevance`
        )
        .then((res) => {
          setLoading(false);
          if (res.data.data.length === 0) {
            Swal.fire({
              type: "error",
              title: "Error",
              text: "¡No hay artículos para filtrar!",
              width: "600px",
            });
            dispatch(setLastArt(true));
          } else if (res.data.data.status === 404) {
            dispatch(setListProducts([]));
            Swal.fire({
              type: "error",
              title: "Error",
              text: res.data.message,
              width: "600px",
            });
          } else {
            dispatch(setListProducts(res.data.data));
          }
        })
        .catch((e) => {
          setLoading(false);
          setLoading(false);
          Swal.fire({
            type: "error",
            title: "Error",
            text: e,
            width: "600px",
          });
        });
    }
  };

  const leerBusqueda = (e) => {
    guardarBusqueda(e.target.value);
  };

  const handlePagination = (event, page) => {
    cardRef.current.style.opacity = 0;
    setTimeout(() => {
      cardRef.current.style.opacity = 1;
    }, 100);

    setDataPage(page - 1);
    setPage(page - 1);
  };

  return (
    <Fragment>
      <Meta
        ogType={"buscador"}
        ogUrl={"localhost:3000" + router.basePath + router.asPath}
      />
      <div className={styles.filter}>
        <span> Filtrar por</span>
        <div
          className={styles.iconFilter}
          onClick={(e) => {
            filterRelevant(e);
          }}
        >
          Más relevantes
          <FaFilter />
        </div>
      </div>
      <div className={styles.inputWrapperSearch}>
        <TextField
          id="outlined-basic"
          label="Buscar productos"
          variant="outlined"
          onChange={(e) => leerBusqueda(e)}
          onKeyPress={(e) => buscarProducto(e)}
          className={styles.inputSearch}
          value={busqueda}
        />
        <MdSearch className={styles.icon} />
      </div>
      <div className={styles.loader}>
        <DotLoader loading={loading} cssOverride={override} size={60} />
      </div>
      {lastArt && list.length !== 0 && loading === false ? (
        <div className={styles.ultimosArt}>Ultimos articulos publicados</div>
      ) : (
        ""
      )}
      <div
        className={styles.cardWrapperProducts}
        ref={cardRef}
        style={{
          transition: "opacity 1s linear",
        }}
      >
        <Cards
          list={list}
          loading={loading}
          page={page}
          rowsPerPage={rowsPerPage}
          setLoading={setLoading}
        />
      </div>

      {list.length !== 0 && loading === false ? (
        <div className={styles.pagination}>
          <Pagination
            count={Math.ceil(list.length / 5)}
            onChange={handlePagination}
            color="primary"
            page={dataPage + 1}
          />
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );
}
