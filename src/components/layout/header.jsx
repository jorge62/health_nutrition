import React, { Fragment } from "react";
import styles from "./styles.module.scss";

export const Header = () => {
  return (
    <Fragment>
      <header className={styles.barra}>
        <div className={styles.wrapperTitle}>
          <span className={styles.title}>Salud </span>
          <span className={styles.title2}> y </span>
          <span className={styles.title3}> Nutricion</span>
        </div>
      </header>
    </Fragment>
  );
};
