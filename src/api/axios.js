import axios from "axios";
const clienteAxios = axios.create({
  baseURL: "https://beta.mejorconsalud.com/wp-json/mc/v3",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});
export default clienteAxios;
