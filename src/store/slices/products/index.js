import { createSlice } from "@reduxjs/toolkit";

export const productsSlice = createSlice({
  name: "products",
  initialState: {
    list: [],
    searchProd: "",
    lastArt: false,
  },
  reducers: {
    setListProducts: (state, action) => {
      state.list = action.payload;
    },
    setSearchProd: (state, action) => {
      state.searchProd = action.payload;
    },
    setLastArt: (state, action) => {
      state.lastArt = action.payload;
    },
  },
});

export const { setListProducts } = productsSlice.actions;
export const { setSearchProd } = productsSlice.actions;
export const { setLastArt } = productsSlice.actions;

export default productsSlice.reducer;
